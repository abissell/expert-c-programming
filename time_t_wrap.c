#include <time.h>
#include <stdio.h>
#include <limits.h>

#define SECS_PER_MIN 60
#define SECS_PER_HOUR (60 * SECS_PER_MIN)
#define SECS_PER_DAY (24 * SECS_PER_HOUR)
#define SECS_PER_MONTH (30 * SECS_PER_DAY)
#define SECS_PER_YEAR (12 * SECS_PER_MONTH)

char *format_time_diff(double diff_time)
{
    int years = (int) (diff_time / SECS_PER_YEAR);
    diff_time %= SECS_PER_YEAR;
    int months = (int) (diff_time / SECS_PER_MONTH);
    diff_time %= SECS_PER_MONTH;
    int days = (int) (diff_time / SECS_PER_DAY);
    diff_time %= SECS_PER_DAY;
    int hours = (int) (diff_time / SECS_PER_HOUR);
    diff_time %= SECS_PER_HOUR;
    int mins = (int) (diff_time / SECS_PER_MIN);
    diff_time %= SECS_PER_MIN;
    int secs = (int) diff_time;
    
    
    

int main()
{
    time_t max_time = (time_t) INT_MAX;
    char *time_str = ctime(&max_time);
    printf("Max time_t is %s", time_str);
    time_t cur_time = time(NULL);
    double diff_time = difftime(max_time, cur_time);
    printf("Time left to time_t wrap is %f\n", diff_time);
}
